export * from './students.service';
import { StudentsService } from './students.service';
export * from './students.serviceInterface';
export * from './user.service';
import { UserService } from './user.service';
export * from './user.serviceInterface';
export const APIS = [StudentsService, UserService];
